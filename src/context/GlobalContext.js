import React, { useState } from "react";
import api from "../services/api";

const GlobalContext = React.createContext();

const GlobalProvider = ({ children }) => {
  const [authenticated, setAuthenticated] = useState(false);
  const [currentConversation, setCurrentConversation] = useState(null);
  const [user, setUser] = useState({
    id: 3,
    name: "Lucas Torres",
    email: "lucas@email.com",
  });

  const handleLogin = (history, user) => {
    api
      .post("/auth/login", user)
      .then((response) => {
        api.defaults.headers.Authorization = response.data.token;
        setUser(response.data.user);
        setAuthenticated(true);
        history.push("/chat-home");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const closeConversation = () => {
    setCurrentConversation(null);
  };

  return (
    <GlobalContext.Provider
      value={{
        user,
        currentConversation,
        setCurrentConversation,
        closeConversation,
        authenticated,
        handleLogin,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
export { GlobalProvider };
