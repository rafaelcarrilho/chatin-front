import React, { useContext, useEffect, useState } from "react";
import GlobalContext from "../../context/GlobalContext";

import Users from "./components/Users";
import Chat from "./components/Chat";

import "./style.css";
import { useHistory } from "react-router-dom";

function Home() {
  const context = useContext(GlobalContext);
  const history = useHistory();

  const [conversation, setConversation] = useState(context.currentConversation);
  useEffect(() => {
    setConversation(context.currentConversation);
  }, [context.currentConversation, conversation]);

  return (
    <div id="home" className="container">
      <div className="user-info">
        <p>
          Welcome back, {context.user.name.split(" ", 1)}!
        </p>
      </div>
      {conversation && <Chat conversation={conversation} />}
      {!conversation && <Users />}
    </div>
  );
}

export default Home;
